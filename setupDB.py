"""
SetupDB is used to create database and initit all its tables.
It allso add some testing data for easy display on web.
"""
from exchange.models.trade import Trade
from exchange.connector import engine, BaseModel, db_session


# delete whole DB
try:
    with db_session() as session:
        meta = BaseModel.metadata
        for table in reversed(meta.sorted_tables):
            session.execute(table.delete())
except Exception as exc:
    print("Failed to delete DB")
    print(str(exc))

# create fresh new database
BaseModel.metadata.create_all(engine)

# add some testing data
with db_session() as session:
    t1 = Trade(id="TR0000000", sellCurrency="USD", sellAmount=100, buyCurrency="EUR", buyAmount=80, rate=1.356)
    t2 = Trade(id="TR0000001", sellCurrency="USD", sellAmount=100, buyCurrency="GBP", buyAmount=65, rate=1.005)
    session.add_all([t1, t2])
