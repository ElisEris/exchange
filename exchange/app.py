import os
from flask import Flask
from flask_redis import FlaskRedis
from flask_wtf.csrf import CSRFProtect

from exchange import constants 


"""Define the exchange application
   app - represent url routing in application
   redis_store - used as a common storage for all threads/processes
                 persistant storage for life span of application
                 mainly for caching fixer.io exchange data"""
app = Flask(constants.AplicationName, template_folder='exchange/templates', static_folder = 'exchange/static', instance_relative_config=False)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
app.config['REDIS_URL'] = constants.RedisConnection
redis_store = FlaskRedis(app)
csrf = CSRFProtect()
# TODO joing DB connection here or leave it in connector.py 
# maybe to join this files completely
with app.app_context():
    # Set global values
    redis_store.currencies = {}
    redis_store.refreshed = 0
    # Initialize globals
    redis_store.init_app(app)
    csrf.init_app(app)

# import all the routes to app
from exchange.routes import exchangeAPI, webAPI  # flake8: noqa
