from sqlalchemy import exc, desc
from time import time
import requests
import uuid
import logging

from exchange.app import redis_store
from exchange.connector import db_session_wrap
from exchange.models.trade import Trade
from exchange import constants


def get_cross_rate(sell, buy):
    """Function for calculating the rate between two currencies
       for rate calculation it use the data from external exchange fixer.io
       1) refresh the curencies and rates if neceserry
       2) if correct it will look up data in redis
       3) then return specified rate as float"""
    sell = sell.upper()
    buy = buy.upper()
    if not validate_cross(sell, buy):
        raise EngineError(400, "Cross is not valid")
    _refresh_currencies()
    baseRate = redis_store.currencies.get(sell, 0)
    quoteRate = redis_store.currencies.get(buy, 0)
    if baseRate == 0 or quoteRate == 0:
        raise EngineError(404, "Rate not found")
    return quoteRate / baseRate


def get_currencies():
    """Return list of all the supported currencies"""
    _refresh_currencies()
    return list(redis_store.currencies.keys())


@db_session_wrap
def get_trades(session):
    """return list of all the trades in order from newest to the oldes"""
    data = session.query(Trade).order_by(desc(Trade.dateBooked)).all()
    return [trade.asdict() for trade in data]


@db_session_wrap
def book_trade(trade_data, session):
    """Book trade to DB
       1) it will validate the trade (for example if price rate is correct/actul)
       2) generate unique trade ID
       3) seve trade to DB, if there is ID collision it will repeat 2)
          (maximali for MaxIdGenerationAttempts times and then it will refuse the trade)
       4) wil return the structure of the booked trade
          (for ensuring client that his trade was booked and in which form)"""
    tradevalidation, status = validate_trade(trade_data)
    if not tradevalidation:
        raise EngineError(403, "Trade is not valid." + status)
    trade = Trade.fromdict(Trade(), trade_data)
    for _ in range(constants.MaxIdGenerationAttempts):  # TODO python retrying here?
        trade.id = _genereate_tradeID()
        try:
            session.add(trade)
            session.flush()
            response = trade.asdict()
            response[constants.ResponseStatus] = constants.StatusBooked
            return response
        except exc.IntegrityError:
            logging.warning(f"'{trade.id}'' already in DB")
    logging.error(f"Cannot generate correct ID for {constants.MaxIdGenerationAttempts} times, aborting")
    session.rollback()
    raise EngineError(505, "Cannot generate correct ID")


def validate_trade(trade):
    """Validate trade
       Data can arrived corrupt or stale so this will protect the exchange
       trade should be booked only when it is valid"""
    status = ""
    try:
        if not validate_cross(trade["sellCurrency"], trade["buyCurrency"]):
            status += "Cross is not valid."
        rate = get_cross_rate(trade["sellCurrency"], trade["buyCurrency"])
        if not rate:
            status += "rate cant be verified."
        if abs(rate - trade["rate"]) > constants.priceTollerance:
            status += "Price changed."
        if trade["sellAmount"] <= 0:
            status += "Sell amount is wrong."
        buyAmount = trade["sellAmount"] * trade["rate"]
        if abs(buyAmount - trade["buyAmount"]) > constants.buyAmountTollerance:
            status += "Buy amount is wrong."
    except KeyError:
        status += "Key data in trade are missing."
    return status == "", status


def validate_cross(base, quote):
    """Validate if the traded cross is correct"""
    if base == quote:
        return False
    if len(base) != 3 or len(quote) != 3:
        return False
    if not base.isalpha() or not quote.isalpha():
        """this will eliminate obviously wrong crosses,
        which will help reduce load of DB"""
        return False
    return True


def _genereate_tradeID():
    """Generate unique trade ID of 9 characters
       app use UUID and then will take only part of it
       this should provide enough randomness
       Note: collision of ID are possible"""
    return "TR" + uuid.uuid4().hex[:7].upper()  # add unique trade value


def _refresh_currencies():
    """Will init data from external exchange fixer.io
       the update of data is only once in interval of time (1 hour for testing purpose)
       For reducint the offload the app conect to fixer only when it dont have data or when
       there will be new update, otherwise it will use data from its redish case
       Note: the number of request for fixer is limited, so for development app use example data"""
    if redis_store.currencies == {} or redis_store.refreshed + constants.DataRefreshRateSeconds < time():
        if constants.RealConnect:
            r = requests.get(url=constants.RatesURL)
            response = r.json()
        else:
            response = constants.SampleRates
        try:
            redis_store.currencies = response["rates"]
            redis_store.refreshed = response["timestamp"]
            if response["rates"] == {}:
                raise EngineError(505, "No data from connection to world exchange")
        except KeyError:
            raise EngineError(505, "No connection to world exchange")


class EngineError(Exception):
    """Representation of Engine error"""
    def __init__(self, code, message):
        super().__init__(message)
        self.statusCode = code
