from flask import  request, render_template, redirect, url_for

from exchange.app import app
from exchange import engine

# TODO move to its own form file
from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField, DecimalField
from wtforms.validators import InputRequired


@app.route('/', methods=['GET'])
def indexPage():
    """Main page or index page of the application
    Will diplay all the already booked trades"""
    okMessage = errorMessage = ""
    try:
        okMessage = request.args['okMessage']
    except:
        pass
    try:
        errorMessage = request.args['errorMessage']
    except:
        pass
    return render_template("tradeHistory.html", trades=engine.get_trades(), okMessage=okMessage, errorMessage=errorMessage)


@app.route('/newTradeForm', methods=['GET', "POST"])
def newTradeForm():
    """Web page for creating new trade to the database
    after submiting a valid form, user is redicect to the main page where the status is displayed"""
    form = NewTrade()
    if form.validate_on_submit():
        trade_data = request.form.to_dict()
        trade_data["sellAmount"] = float(trade_data["sellAmount"])
        trade_data["buyAmount"] = float(trade_data["buyAmount"])
        trade_data["rate"] = float(trade_data["rate"])
        try:
            engine.book_trade(trade_data)
            return redirect(url_for(".indexPage", okMessage="Trade Booked", errorMessage=""))
        except engine.EngineError as ee:
            return redirect(url_for(".indexPage", okMessage="", errorMessage=str(ee)))
    ccys = engine.get_currencies()
    ccys.sort()
    ccys = [(ccy, ccy) for ccy in ccys]
    ccys.insert(0, ("", ""))
    form.sellCurrency.choices = ccys
    form.buyCurrency.choices = ccys
    return render_template("newTrade.html", form=form)


# TODO move to its own form file
class NonValidatingSelectField(SelectField):
    def pre_validate(self, form):
        pass


class NewTrade(FlaskForm):
    sellCurrency = NonValidatingSelectField('Sell Currency', choices=[])
    sellAmount = DecimalField('Sell Amount', places=2, validators=[InputRequired()])
    buyCurrency = NonValidatingSelectField('Buy Currency', choices=[])
    buyAmount = DecimalField('Buy Amount', places=2, validators=[InputRequired()])
    rate = DecimalField('Rate', places=6, validators=[InputRequired()])
    trade = SubmitField('Book Trade')
