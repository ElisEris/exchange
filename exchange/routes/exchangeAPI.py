from flask import abort, jsonify, request

from exchange.app import app, csrf
from exchange import engine


@app.route('/trades', methods=['GET'])
@csrf.exempt
def get_trades():
    """Will return all the trades in DB
    TODO: will be nice to add some trade listing
         which will help oflload the load for lot of data"""
    return jsonify(engine.get_trades())


@app.route('/newTrade', methods=['POST'])
@csrf.exempt
def new_trade():
    """Excract data from request and call booking the trade"""
    trade_data = request.get_json()
    try:
        status = engine.book_trade(trade_data)
        return jsonify(status)
    except engine.EngineError as ee:
        abort(ee.statusCode, {"error": str(ee)})


@app.route('/getCurrencies', methods=['GET'])
@csrf.exempt
def get_currencies():
    """Return list of all the currecnies this exchenge support
       the list is from supported data from external exchange fixer.io
       curencies are stored in redis (if data not in cache will be initialised from fixer)"""
    try:
        return jsonify(engine.get_currencies())
    except engine.EngineError as ee:
        abort(ee.statusCode, {"error": str(ee)})


@app.route('/getCrossRate/<string:base>/<string:quote>', methods=['GET'])
@csrf.exempt
def get_cross_rate(base, quote):
    """Function for calculating the rate between two currencies
     1) it validate the cross,
     2) call for rate caltulation"""
    try:
        rate = engine.get_cross_rate(base, quote)
        return jsonify({"rate": rate})
    except engine.EngineError as ee:
        abort(ee.statusCode, {"error": str(ee)})
