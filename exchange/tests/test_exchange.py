import unittest
import json

from exchange.app import app
from exchange import constants
from exchange.engine import get_cross_rate, get_currencies, get_trades, book_trade, \
                validate_cross, validate_trade, _genereate_tradeID, _refresh_currencies, EngineError

class TestExchange(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.redis_store = constants.SampleRates["rates"]
        constants.RealConnect = False

    #not needed now
    #def tearDown(self):

    def test_crossValidation(self):
        self.assertFalse(validate_cross("", "USD"))
        self.assertFalse(validate_cross("USD", ""))
        self.assertFalse(validate_cross("USD", "USD"))
        self.assertFalse(validate_cross("US1", "USD"))
        self.assertFalse(validate_cross("USD", "US1"))
        self.assertFalse(validate_cross("USDD", "EUR"))
        self.assertFalse(validate_cross("USD", "EURS"))
        self.assertTrue(validate_cross("USD", "EUR"))

    def test_validate_trade(self):
        trade = {"sellAmount":100, "buyCurrency":"EUR", "buyAmount":80, "rate":1.356}
        status, errors = validate_trade(trade)
        self.assertFalse(status)
        self.assertEqual(errors, "Key data in trade are missing.")

        trade = {"sellCurrency":"USD", "sellAmount":100, "buyCurrency":"EIR", "buyAmount":80, "rate":1.356}
        try:
            validate_trade(trade)
            self.assertTrue(False)
        except EngineError as exc:
            self.assertEqual(404, exc.statusCode)
            self.assertEqual("Rate not found", str(exc))

        trade = {"sellCurrency":"XXX", "sellAmount":100, "buyCurrency":"EUR", "buyAmount":80, "rate":1.356}
        try:
            validate_trade(trade)
            self.assertTrue(False)
        except EngineError as exc:
            self.assertEqual(404, exc.statusCode)
            self.assertEqual("Rate not found", str(exc))

        trade = {"sellCurrency":"USD", "sellAmount":100, "buyCurrency":"EUR", "buyAmount":80, "rate":1.356}
        status, errors = validate_trade(trade)
        self.assertFalse(status)
        self.assertEqual(errors, "Price changed.Buy amount is wrong.")       

        trade = {"sellCurrency":"USD", "sellAmount":100, "buyCurrency":"GBP", "buyAmount":80, "rate":0.77190295}
        status, errors = validate_trade(trade)
        self.assertFalse(status)
        self.assertEqual(errors, "Buy amount is wrong.")

        trade = {"sellCurrency":"USD", "sellAmount":0, "buyCurrency":"GBP", "buyAmount":0, "rate":0.77190295}
        status, errors = validate_trade(trade)
        self.assertFalse(status)
        self.assertEqual(errors, "Sell amount is wrong.")


        trade = {"sellCurrency":"USD", "sellAmount":-100, "buyCurrency":"GBP", "buyAmount":-77.190295, "rate":0.77190295}
        status, errors = validate_trade(trade)
        self.assertFalse(status)
        self.assertEqual(errors, "Sell amount is wrong.")

        trade = {"sellCurrency":"USD", "sellAmount":100, "buyCurrency":"GBP", "buyAmount":77.190295, "rate":0.77190295}
        status, errors = validate_trade(trade)
        self.assertTrue(status)
        self.assertEqual(errors, "")

    def test_genereate_tradeID(self):
        self.assertEqual("TR", _genereate_tradeID()[:2])
        self.assertEqual(9, len(_genereate_tradeID()))
        """Assert if it generate random ID. 
           there is a change (almost 0) for colision
           but I am willing to risk it"""
        ids = set()
        for _ in range(0,10):
            ids.add(_genereate_tradeID())
        self.assertEqual(10, len(ids))

    def test_get_currencies(self):
        expectedRate = constants.SampleRates["rates"]["GBP"] / constants.SampleRates["rates"]["USD"]
        self.assertEqual(get_cross_rate("USD", "GBP"), expectedRate)

        # no cashe
        self.app.redis_store = {}
        self.assertEqual(get_cross_rate("USD", "GBP"), expectedRate)


    def test_get_cross_rate(self):
        self.assertEqual(get_currencies(), list(constants.SampleRates["rates"].keys()))

        # no cashe
        self.app.redis_store = {}
        self.assertEqual(get_currencies(), list(constants.SampleRates["rates"].keys()))




    def atest_getCurrencies_ok(self):
        rv = self.app.get('/getCurrencies')
        responce = json.loads(rv.data)
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(responce, list(constants.SampleRates["rates"].keys()))

    def atest_getCurrencies_emptyCache(self):
        self.app.redis_store = {}
        rv = self.app.get('/getCurrencies')
        responce = json.loads(rv.data)
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(responce, list(constants.SampleRates["rates"].keys()))

    def atest_getCrossRate(self):
        rv = self.app.get("/getCrossRate/USD/GBP")
        responce = json.loads(rv.data)
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(responce["rate"], constants.SampleRates["rates"]["GBP"] / constants.SampleRates["rates"]["USD"])

        rv = self.app.get("/getCrossRate/usd/gBp")
        responce = json.loads(rv.data)
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(responce["rate"], constants.SampleRates["rates"]["GBP"] / constants.SampleRates["rates"]["USD"])

    def atest_getCrossRate_wrongCross(self):
        rv = self.app.get("/getCrossRate/USsD/GBP")
        self.assertEqual(rv.status_code, 400)
        rv = self.app.get("/getCrossRate/US1/GBP")
        self.assertEqual(rv.status_code, 400)
        rv = self.app.get("/getCrossRate/USD-GBP")
        self.assertEqual(rv.status_code, 404)

if __name__ == '__main__':
    unittest.main()
