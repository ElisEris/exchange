import datetime
from sqlalchemy import Column, String, DateTime, Float  # Integer

from exchange.connector import BaseModel


class Trade(BaseModel):
    __tablename__ = 'Trade'

    id = Column(
        String(9),
        name='id',
        nullable=False,
        primary_key=True
    )

    sellCurrency = Column(
        String(2),
        name='Sell Currency',
        nullable=False,
    )
    sellAmount = Column(
        Float(),
        name='Sell Amount',
        nullable=False,
    )

    buyCurrency = Column(
        String(2),
        name='Buy Currency',
        nullable=False,
    )

    buyAmount = Column(
        Float(),
        name='Buy Amount',
        nullable=False,
    )

    rate = Column(
        Float(),
        name='Rate',
        nullable=False
    )

    dateBooked = Column(
        DateTime,
        name='Date Booked',
        nullable=False,
        default=datetime.datetime.utcnow
    )
