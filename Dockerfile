FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
RUN apk --update add bash nano
COPY ./requirements.txt .
RUN pip install -r ./requirements.txt
COPY . .
RUN python3 setupDB.py
CMD python3 exchange.py